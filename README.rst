=======
sb_style
=======

sb_style is django-cms plugin which is part of SBL (Service bussiness library, https://vits.pro/dev/).

This plugin adds style to child plugins.


Installation
============

sb_style requires sb_core package: https://bitbucket.org/vivazzi/sb_core/

There is no sb_style in PyPI, so you can install this package from bitbucket repository only.

::
 
     $ pip install hg+https://bitbucket.org/vivazzi/sb_style


Configuration 
=============

1. Add "sb_style" to INSTALLED_APPS after "sb_core" ::

    INSTALLED_APPS = (
        ...
        'sb_core',
        'sb_style',
        ...
    )

2. Run `python manage.py migrate` to create the sb_style models.