from django.apps import AppConfig

from django.utils.translation import gettext_lazy as _


class SBStyleConfig(AppConfig):
    name = 'sb_style'
    verbose_name = _('Style')
