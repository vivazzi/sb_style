from django import forms

from sb_core import settings
from sb_style.models import SBStyle


class SBStyleForm(forms.ModelForm):
    class Meta:
        model = SBStyle
        exclude = ()

    class Media:
        css = {'all': ['sb_style/css/sb_style_admin.css', ]}
        js = [settings.JQUERY_URL, 'sb_core/js/sb_library.js', 'sb_style/js/sb_style_admin.js']
