from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.db import models
from cms.models import CMSPlugin

from sb_core.constants import title_ht, shadow_ht, color_ht
from sb_core.fields import ColorField


class SBStyle(CMSPlugin):
    title = models.CharField('Название', max_length=255, blank=True, help_text=title_ht)

    padding_left = models.PositiveSmallIntegerField('Слева', blank=True, null=True)
    padding_right = models.PositiveSmallIntegerField('Справа', blank=True, null=True)
    padding_top = models.PositiveSmallIntegerField('Сверху', blank=True, null=True)
    padding_bottom = models.PositiveSmallIntegerField('Снизу', blank=True, null=True)

    margin_left = models.SmallIntegerField('Слева', blank=True, null=True)
    margin_right = models.SmallIntegerField('Справа', blank=True, null=True)
    margin_top = models.SmallIntegerField('Сверху', blank=True, null=True)
    margin_bottom = models.SmallIntegerField('Снизу', blank=True, null=True)

    is_shadow = models.BooleanField('Добавить тень?', default=False)
    shadow_pars = models.CharField('Параметры тени', max_length=100, blank=True, null=True, help_text=shadow_ht)

    is_border = models.BooleanField('Добавить границу?', default=False)
    border_size = models.PositiveSmallIntegerField('Размер границ, пикс', null=True, blank=True, validators=[MinValueValidator(1), ])
    border_color = ColorField('Цвет границ', blank=True, help_text=color_ht)

    bg_color = ColorField('Цвет фона', blank=True, help_text=color_ht)

    max_width = models.PositiveSmallIntegerField('Максимальная ширина, пикс', blank=True, null=True)

    LEFT = 'left'
    CENTER = 'center'
    RIGHT = 'right'
    ALIGNMENT_CHOICES = ((LEFT, 'влево'),
                         (CENTER, 'по центру'),
                         (RIGHT, 'вправо'))

    alignment = models.CharField('Выравнивание', max_length=10, blank=True, null=True, choices=ALIGNMENT_CHOICES,
                                 help_text='Выравнивает блок стиля (в некоторых случаях работает только при указании максимальной ширины)')

    @property
    def style(self):
        style = ''
        if self.padding_left: style += 'padding-left: {}px;'.format(self.padding_left)
        if self.padding_right: style += 'padding-right: {}px;'.format(self.padding_right)
        if self.padding_bottom: style += 'padding-bottom: {}px;'.format(self.padding_bottom)
        if self.padding_top: style += 'padding-top: {}px;'.format(self.padding_top)

        if self.margin_left: style += 'margin-left: {}px;'.format(self.margin_left)
        if self.margin_right: style += 'margin-right: {}px;'.format(self.margin_right)
        if self.margin_top: style += 'margin-top: {}px;'.format(self.margin_top)
        if self.margin_bottom: style += 'margin-bottom: {}px;'.format(self.margin_bottom)

        if self.is_border: style += 'border: {}px solid {};'.format(self.border_size, self.border_color)
        if self.is_shadow: style += 'box-shadow: {};'.format(self.shadow_pars)
        if self.bg_color: style += 'background-color: {};'.format(self.bg_color)

        if self.max_width and not self.alignment: style += 'max-width: {}px;'.format(self.max_width)

        if self.alignment: style += 'display:inline-block;'
        return style

    @property
    def get_wr_margin(self):
        style = ''
        if self.alignment == self.LEFT or self.alignment == self.CENTER: style += 'margin-right: auto;'
        if self.alignment == self.RIGHT or self.alignment == self.CENTER: style += 'margin-left: auto;'

        style += 'text-align: {};'.format(self.alignment)

        if self.max_width: style += 'max-width: {}px;'.format(self.max_width)
        return style

    def clean(self):
        if self.is_shadow and not self.shadow_pars:
            raise ValidationError('При использовании тени укажите значение в поле "Параметры тени"')

        if self.is_border and not (self.border_size and self.border_color):
            raise ValidationError('При использовании границы укажите значения в обоих полях: "Размер границ", "Цвет границ"')

    def __str__(self):
        return self.title or ''

    class Meta:
        db_table = 'sb_style'
        verbose_name = 'Стиль'
        verbose_name_plural = 'Стили'


class SBSpacer(CMSPlugin):
    height = models.PositiveSmallIntegerField('Высота, пикс')

    def __str__(self):
        return '{} пикс.'.format(self.height)

    class Meta:
        db_table = 'sb_spacer'
        verbose_name = 'Распорка'
        verbose_name_plural = 'Распорки'