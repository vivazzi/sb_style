from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_style', '0006_auto_20160809_1301'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbspacer',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='sb_style_sbspacer', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='sbstyle',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='sb_style_sbstyle', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE),
        ),
    ]
