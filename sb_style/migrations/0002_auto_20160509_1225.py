from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_style', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbstyle',
            name='max_width',
            field=models.PositiveSmallIntegerField(null=True, verbose_name='\u041c\u0430\u043a\u0441\u0438\u043c\u0430\u043b\u044c\u043d\u0430\u044f \u0448\u0438\u0440\u0438\u043d\u0430, \u043f\u0438\u043a\u0441', blank=True),
        ),
    ]
