from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0013_urlconfrevision'),
    ]

    operations = [
        migrations.CreateModel(
            name='SBSpacer',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE)),
                ('height', models.PositiveSmallIntegerField(verbose_name='\u0412\u044b\u0441\u043e\u0442\u0430, \u043f\u0438\u043a\u0441')),
            ],
            options={
                'db_table': 'sb_spacer',
                'verbose_name': '\u0420\u0430\u0441\u043f\u043e\u0440\u043a\u0430',
                'verbose_name_plural': '\u0420\u0430\u0441\u043f\u043e\u0440\u043a\u0438',
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='SBStyle',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE)),
                ('title', models.CharField(help_text='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0435\u0442\u0441\u044f \u0442\u043e\u043b\u044c\u043a\u043e \u0432 \u0430\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u0438\u0432\u043d\u043e\u0439 \u0447\u0430\u0441\u0442\u0438', max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True)),
                ('padding_left', models.PositiveSmallIntegerField(null=True, verbose_name='\u0421\u043b\u0435\u0432\u0430', blank=True)),
                ('padding_right', models.PositiveSmallIntegerField(null=True, verbose_name='\u0421\u043f\u0440\u0430\u0432\u0430', blank=True)),
                ('padding_top', models.PositiveSmallIntegerField(null=True, verbose_name='\u0421\u0432\u0435\u0440\u0445\u0443', blank=True)),
                ('padding_bottom', models.PositiveSmallIntegerField(null=True, verbose_name='\u0421\u043d\u0438\u0437\u0443', blank=True)),
                ('margin_left', models.SmallIntegerField(null=True, verbose_name='\u0421\u043b\u0435\u0432\u0430', blank=True)),
                ('margin_right', models.SmallIntegerField(null=True, verbose_name='\u0421\u043f\u0440\u0430\u0432\u0430', blank=True)),
                ('margin_top', models.SmallIntegerField(null=True, verbose_name='\u0421\u0432\u0435\u0440\u0445\u0443', blank=True)),
                ('margin_bottom', models.SmallIntegerField(null=True, verbose_name='\u0421\u043d\u0438\u0437\u0443', blank=True)),
                ('is_shadow', models.BooleanField(default=False, verbose_name='\u0414\u043e\u0431\u0430\u0432\u0438\u0442\u044c \u0442\u0435\u043d\u044c?')),
                ('shadow_pars', models.CharField(help_text='<\u0441\u0434\u0432\u0438\u0433 \u043f\u043e x> <\u0441\u0434\u0432\u0438\u0433 \u043f\u043e y> <\u0440\u0430\u0434\u0438\u0443\u0441 \u0440\u0430\u0437\u043c\u044b\u0442\u0438\u044f> <\u0440\u0430\u0441\u0442\u044f\u0436\u0435\u043d\u0438\u0435> <\u0446\u0432\u0435\u0442></br>\u041d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: 0 0 10px 0 rgba(0, 0, 0, 0.5), 2px 2px 7px 1px rgba(0, 0, 0, 0.4)', max_length=100, null=True, verbose_name='\u041f\u0430\u0440\u0430\u043c\u0435\u0442\u0440\u044b \u0442\u0435\u043d\u0438', blank=True)),
                ('is_border', models.BooleanField(default=False, verbose_name='\u0414\u043e\u0431\u0430\u0432\u0438\u0442\u044c \u0433\u0440\u0430\u043d\u0438\u0446\u0443?')),
                ('border_size', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='\u0420\u0430\u0437\u043c\u0435\u0440 \u0433\u0440\u0430\u043d\u0438\u0446, \u043f\u0438\u043a\u0441', validators=[django.core.validators.MinValueValidator(1)])),
                ('border_color', models.CharField(max_length=50, blank=True, help_text='\u041f\u0440\u0438\u043c\u0435\u0440\u044b: #545454, rgb(9,18,12), rgba(20,0,0,0.5)<br/>\u0438\u043b\u0438 rgba(0,0,0,0) \u0434\u043b\u044f \u043f\u0440\u043e\u0437\u0440\u0430\u0447\u043d\u043e\u0433\u043e \u0444\u043e\u043d\u0430', null=True, verbose_name='\u0426\u0432\u0435\u0442 \u0433\u0440\u0430\u043d\u0438\u0446')),
                ('bg_color', models.CharField(help_text='\u041f\u0440\u0438\u043c\u0435\u0440\u044b: #545454, rgb(9,18,12), rgba(20,0,0,0.5)<br/>\u0438\u043b\u0438 rgba(0,0,0,0) \u0434\u043b\u044f \u043f\u0440\u043e\u0437\u0440\u0430\u0447\u043d\u043e\u0433\u043e \u0444\u043e\u043d\u0430', max_length=50, null=True, verbose_name='\u0426\u0432\u0435\u0442 \u0444\u043e\u043d\u0430', blank=True)),
                ('max_width', models.PositiveSmallIntegerField(null=True, verbose_name='\u041c\u0430\u043a\u0441\u0438\u043c\u0430\u043b\u044c\u043d\u0430\u044f \u0448\u0438\u0440\u0438\u043d\u0430', blank=True)),
                ('alignment', models.CharField(choices=[('left', '\u0432\u043b\u0435\u0432\u043e'), ('center', '\u043f\u043e \u0446\u0435\u043d\u0442\u0440\u0443'), ('right', '\u0432\u043f\u0440\u0430\u0432\u043e')], max_length=10, blank=True, help_text='\u0412\u044b\u0440\u0430\u0432\u043d\u0438\u0432\u0430\u0435\u0442 \u0431\u043b\u043e\u043a \u0441\u0442\u0438\u043b\u044f (\u0440\u0430\u0431\u043e\u0442\u0430\u0435\u0442 \u043f\u0440\u0438 \u0443\u043a\u0430\u0437\u0430\u043d\u0438\u0438 \u043c\u0430\u043a\u0441\u0438\u043c\u0430\u043b\u044c\u043d\u043e\u0439 \u0448\u0438\u0440\u0438\u043d\u044b)', null=True, verbose_name='\u0412\u044b\u0440\u0430\u0432\u043d\u0438\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'db_table': 'sb_style',
                'verbose_name': '\u0421\u0442\u0438\u043b\u044c',
                'verbose_name_plural': '\u0421\u0442\u0438\u043b\u0438',
            },
            bases=('cms.cmsplugin',),
        ),
    ]
