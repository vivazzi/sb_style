from cms.plugin_pool import plugin_pool
from cms.plugin_base import CMSPluginBase

from sb_core.constants import DECOR
from sb_style.forms import SBStyleForm
from sb_style.models import SBStyle, SBSpacer


class SBSpacerPlugin(CMSPluginBase):
    module = DECOR
    model = SBSpacer
    name = 'Распорка'
    render_template = 'sb_style/sb_spacer.html'
    text_enabled = True

plugin_pool.register_plugin(SBSpacerPlugin)


class SBClearfixPlugin(CMSPluginBase):
    module = DECOR
    name = 'Разделитель'
    render_template = 'sb_style/sb_clearfix.html'
    text_enabled = True

plugin_pool.register_plugin(SBClearfixPlugin)


class SBStylePlugin(CMSPluginBase):
    module = DECOR
    model = SBStyle
    name = 'Стиль'
    form = SBStyleForm
    allow_children = True
    render_template = 'sb_style/sb_style.html'

    fieldsets = (
        ('', {
            'fields': ('title', )
        }),
        ('Внутренний отступ (в пикс.)', {
            'fields': (('padding_left', 'padding_right', 'padding_top', 'padding_bottom'), )
        }),
        ('Внешний отступ (в пикс.)', {
            'fields': (('margin_left', 'margin_right', 'margin_top', 'margin_bottom'), )
        }),
        ('Тень и граница', {
            'fields': (('is_shadow', 'shadow_pars'),
                       ('is_border', 'border_size', 'border_color'))
        }),
        ('Другое', {
            'fields': ('bg_color', 'max_width', 'alignment')
        }),
    )

plugin_pool.register_plugin(SBStylePlugin)
